package eu.kprod.jira.searchers;

import junit.framework.TestCase;
import org.junit.*;

public class NaturalWithNullTest extends TestCase {

	private static String string250;
	private static String string256;
	private static String string259;

	@Before
	public void setUp() throws Exception {
		// reset string before every text
		StringBuilder strBdr = new StringBuilder();
		for (int i = 0; i < 250; i++) {
			strBdr.append("i");
		}
		string250 = strBdr.toString();
		string256 = string250 + "123456";
		string259 = string250 + "123456789";
	}

	@After
	public void tearDown() throws Exception {
		// after each test
	}

	@Test
	public void testNaturalWithNull() {
		String strLow = string250.toLowerCase();
		String strBig = string250.toUpperCase();

		assertTrue(0 < NaturalWithNullComparator.CASE_SENSITIVE_ORDER.compare(strLow,null));
		assertTrue(0 > NaturalWithNullComparator.CASE_INSENSITIVE_ORDER.compare(null,strBig));
		assertEquals(0, NaturalWithNullComparator.CASE_SENSITIVE_ORDER.compare(null, null));
	}

	@Test
	public void testNaturalWithNullCASESENSITIVE() {
		String strLow = string250.toLowerCase();
		String strBig = string250.toUpperCase();

		assertTrue(0 < NaturalWithNullComparator.CASE_SENSITIVE_ORDER.compare(strLow, strBig));
		assertTrue(0 > NaturalWithNullComparator.CASE_SENSITIVE_ORDER.compare(strBig, strLow));
		assertEquals(0, NaturalWithNullComparator.CASE_SENSITIVE_ORDER.compare(strBig, strBig));
		assertEquals(0, NaturalWithNullComparator.CASE_SENSITIVE_ORDER.compare(strLow, strLow));
	}

	@Test
	public void testNaturalWithNullCASESENSITIVE255Limit() {
		String strLow = string256.toLowerCase();
		String strBig = string259.toUpperCase();

		assertTrue(NaturalWithNullComparator.CASE_SENSITIVE_ORDER.getComparLimit() < strLow.length());
		assertTrue(NaturalWithNullComparator.CASE_SENSITIVE_ORDER.getComparLimit() < strBig.length());

		assertTrue(0 < NaturalWithNullComparator.CASE_SENSITIVE_ORDER.compare(strLow, strBig));
		assertTrue(0 > NaturalWithNullComparator.CASE_SENSITIVE_ORDER.compare(strBig, strLow));

		assertTrue(0 > NaturalWithNullComparator.CASE_SENSITIVE_ORDER.compare(strBig, strBig.toLowerCase()));
		assertTrue(0 < NaturalWithNullComparator.CASE_SENSITIVE_ORDER.compare(strBig.toLowerCase(), strBig));

		assertEquals(0, NaturalWithNullComparator.CASE_SENSITIVE_ORDER.compare(strBig, strBig));
		assertEquals(0, NaturalWithNullComparator.CASE_SENSITIVE_ORDER.compare(strLow, strLow));
	}

	@Test
	public void testNaturalWithNullCASEINSENSITIVE() {
		String strLow = string250.toLowerCase();
		String strBig = string250.toUpperCase();

		assertEquals(0, NaturalWithNullComparator.CASE_INSENSITIVE_ORDER.compare(strLow, strBig));
		assertEquals(0, NaturalWithNullComparator.CASE_INSENSITIVE_ORDER.compare(strBig, strLow));
		assertEquals(0, NaturalWithNullComparator.CASE_INSENSITIVE_ORDER.compare(strBig, strBig));
		assertEquals(0, NaturalWithNullComparator.CASE_INSENSITIVE_ORDER.compare(strLow, strLow));
	}

	@Test
	public void testNaturalWithNullCASEINSENSITIVE255Limit() {
		String strLow = string256.toLowerCase();
		String strBig = string259.toUpperCase();

		assertTrue(NaturalWithNullComparator.CASE_SENSITIVE_ORDER.getComparLimit() < strLow.length());
		assertTrue(NaturalWithNullComparator.CASE_SENSITIVE_ORDER.getComparLimit() < strBig.length());

		assertEquals(0, NaturalWithNullComparator.CASE_INSENSITIVE_ORDER.compare(strLow, strBig));
		assertEquals(0, NaturalWithNullComparator.CASE_INSENSITIVE_ORDER.compare(strBig, strLow));
		assertEquals(0, NaturalWithNullComparator.CASE_INSENSITIVE_ORDER.compare(strBig, strBig));
		assertEquals(0, NaturalWithNullComparator.CASE_INSENSITIVE_ORDER.compare(strLow, strLow));
	}
}
