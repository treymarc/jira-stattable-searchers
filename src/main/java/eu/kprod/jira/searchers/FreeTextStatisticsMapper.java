package eu.kprod.jira.searchers;

import static com.atlassian.jira.issue.search.util.SearchRequestAddendumBuilder.appendAndClause;
import static com.atlassian.jira.issue.search.util.SearchRequestAddendumBuilder.appendAndNotClauses;

import java.util.Comparator;

import com.atlassian.jira.issue.customfields.searchers.transformer.CustomFieldInputHelper;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchRequestAppender;
import com.atlassian.jira.issue.search.util.SearchRequestAddendumBuilder;
import com.atlassian.jira.issue.statistics.StatisticsMapper;
import com.atlassian.jira.jql.builder.JqlClauseBuilder;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.dbc.Assertions;
import com.atlassian.query.operator.Operator;

/**
 * Implementing StatisticsMapper for Text/Textarea custom fields for usage with
 * 2D Field Statistics gadget.
 * 
 * Most of the implementation is inspired by (view source)
 * 
 * com.atlassian.jira.issue.statistics.ProjectStatisticsMapper
 * com.atlassian.jira.issue.statistics.CustomFieldProjectStatisticsMapper
 * com.atlassian.jira.issue.customfields.searchers.ProjectSearcher
 * com.atlassian.jira.issue.statistics.LabelsStatisticsMapper
 * 
 * @author Peter Csiba
 * @author Jozef Kotlar
 * @author Marc Trey Add requestAppender
 */
public class FreeTextStatisticsMapper implements StatisticsMapper<String>, SearchRequestAppender.Factory<String> {

	protected CustomField				customField;
	protected CustomFieldInputHelper	customFieldInputHelper;
	protected JiraAuthenticationContext	authenticationContext;

	/**
	 * 
	 * @param customField
	 * @param customFieldInputHelper
	 * @param authenticationContext
	 */
	public FreeTextStatisticsMapper(CustomField customField, final JiraAuthenticationContext authenticationContext,
			final CustomFieldInputHelper customFieldInputHelper) {
		this.customField = customField;
		this.customFieldInputHelper = customFieldInputHelper;
		this.authenticationContext = authenticationContext;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		final String that = ((FreeTextStatisticsMapper) o).getDocumentConstant();

		return ((getDocumentConstant() != null) ? getDocumentConstant().equals(that) : that == null);
	}

	@Override
	public Comparator<String> getComparator() {
		return NaturalWithNullComparator.CASE_INSENSITIVE_ORDER;
	}

	@Override
	public String getDocumentConstant() {
		return customField.getId();
	}

	@Deprecated
	@Override
	public SearchRequest getSearchUrlSuffix(String value, SearchRequest searchRequest) {
		return getSearchRequestAppender().appendInclusiveSingleValueClause(value, searchRequest);
	}

	/**
	 * for example in ProjectSearcher only project key is stored in customfield
	 * and in this method it should be converted to ProjectObject in our case
	 * the stored value (String) is equal represented object
	 */
	@Override
	public String getValueFromLuceneField(String documentValue) {
		return documentValue;
	}

	@Override
	public int hashCode() {
		return ((getDocumentConstant() != null) ? getDocumentConstant().hashCode() : 0);
	}

	@Override
	public boolean isFieldAlwaysPartOfAnIssue() {
		return false;
	}

	@Override
	public boolean isValidValue(String value) {
		return true;
	}

	/**
	 * @since v6.0
	 */
	@Override
	public SearchRequestAppender<String> getSearchRequestAppender() {
		return new SelectOptionSearchRequestAppender(customFieldInputHelper.getUniqueClauseName(authenticationContext.getLoggedInUser(),
				customField.getClauseNames().getPrimaryName(), customField.getName()), Operator.LIKE);
	}

	static class SelectOptionSearchRequestAppender implements SearchRequestAddendumBuilder.AddendumCallback<String>, SearchRequestAppender<String> {
		final String	clauseName;
		private Operator	operator;

		public SelectOptionSearchRequestAppender(String clauseName, Operator operator) {
			this.clauseName = Assertions.notNull("clauseName", clauseName);
			this.operator = operator;
		}

		@Override
		public void appendNonNullItem(String value, JqlClauseBuilder clauseBuilder) {
			clauseBuilder.addStringCondition(clauseName, operator, value);
		}

		@Override
		public void appendNullItem(JqlClauseBuilder clauseBuilder) {
			clauseBuilder.addEmptyCondition(clauseName);
		}

		@Override
		public SearchRequest appendInclusiveSingleValueClause(String value, SearchRequest searchRequest) {
			return appendAndClause(value, searchRequest, this);
		}

		@Override
		public SearchRequest appendExclusiveMultiValueClause(Iterable<? extends String> values, SearchRequest searchRequest) {
			return appendAndNotClauses(values, searchRequest, this);
		}
	}

}
