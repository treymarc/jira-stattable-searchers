package eu.kprod.jira.searchers;

import static com.atlassian.jira.issue.search.util.SearchRequestAddendumBuilder.appendAndClause;
import static com.atlassian.jira.issue.search.util.SearchRequestAddendumBuilder.appendAndNotClauses;
import static com.atlassian.jira.util.dbc.Assertions.notNull;

import java.util.Comparator;

import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.searchers.transformer.CustomFieldInputHelper;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchRequestAppender;
import com.atlassian.jira.issue.search.util.SearchRequestAddendumBuilder;
import com.atlassian.jira.issue.statistics.StatisticsMapper;
import com.atlassian.jira.jql.builder.JqlClauseBuilder;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.dbc.Assertions;
import com.atlassian.query.operator.Operator;

/**
 * StatisticsMapper for Cascade custom fields in 2D Field Statistics gadget.
 * jira 6.x
 * 
 * @author Marc Trey
 */
public class CascadingSelectStatisticsMapper implements StatisticsMapper<Option>, SearchRequestAppender.Factory<Option> {

	private CustomField					customField;
	private OptionsManager				optionsManager;
	private JiraAuthenticationContext	authenticationContext;
	private CustomFieldInputHelper		customFieldInputHelper;

	/**
	 * 
	 * @param customField
	 * @param optionsManager
	 * @param authenticationContext
	 * @param customFieldInputHelper
	 */
	public CascadingSelectStatisticsMapper(CustomField customField, OptionsManager optionsManager,
			final JiraAuthenticationContext authenticationContext, final CustomFieldInputHelper customFieldInputHelper) {

		this.optionsManager = optionsManager;
		this.authenticationContext = authenticationContext;
		this.customFieldInputHelper = customFieldInputHelper;
		this.customField = notNull("customField", customField);
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		final CascadingSelectStatisticsMapper that = (CascadingSelectStatisticsMapper) o;

		if (!this.customField.getClauseNames().equals(that.customField.getClauseNames())) {
			return false;
		}
		if (!customField.getId().equals(that.customField.getId())) {
			return false;
		}

		return true;
	}

	@Override
	public Comparator<Option> getComparator() {
		return new Comparator<Option>() {
			@Override
			public int compare(Option o1, Option o2) {
				if (o1 == null && o2 == null) {
					return 0;
				} else if (o1 == null) {
					return 1;
				} else if (o2 == null) {
					return -1;
				}
				return NaturalWithNullComparator.CASE_INSENSITIVE_ORDER.compare(o1.getValue(), o2.getValue());
			}

		};
	}

	@Override
	public String getDocumentConstant() {
		return customField.getId();
	}

	@Deprecated
	@Override
	public SearchRequest getSearchUrlSuffix(Option option, SearchRequest searchRequest) {
		return getSearchRequestAppender().appendInclusiveSingleValueClause(option, searchRequest);
	}

	/**
	 * Get the option value from the option id stored in the Document
	 */
	@Override
	public Option getValueFromLuceneField(String documentValue) {
		try {
			return optionsManager.findByOptionId(Long.valueOf(documentValue));
		} catch (NumberFormatException e) {
			return null;
		}
	}

	/**
	 * @since v6.0
	 */
	@Override
	public SearchRequestAppender<Option> getSearchRequestAppender() {
		return new SelectOptionSearchRequestAppender(customFieldInputHelper.getUniqueClauseName(authenticationContext.getLoggedInUser(),
				customField.getClauseNames().getPrimaryName(), customField.getName()));
	}

	@Override
	public int hashCode() {
		int result = customField.getId().hashCode();
		result = 31 * result + customField.getClauseNames().hashCode();
		return result;
	}

	@Override
	public boolean isFieldAlwaysPartOfAnIssue() {
		return false;
	}

	@Override
	public boolean isValidValue(Option value) {
		return true;
	}

	static class SelectOptionSearchRequestAppender implements SearchRequestAddendumBuilder.AddendumCallback<Option>, SearchRequestAppender<Option> {
		final String	clauseName;

		public SelectOptionSearchRequestAppender(String clauseName) {
			this.clauseName = Assertions.notNull("clauseName", clauseName);
		}

		@Override
		public void appendNonNullItem(Option value, JqlClauseBuilder clauseBuilder) {
			clauseBuilder.addFunctionCondition(clauseName, Operator.IN, "cascadeOption", value.getValue());
		}

		@Override
		public void appendNullItem(JqlClauseBuilder clauseBuilder) {
			clauseBuilder.addEmptyCondition(clauseName);
		}

		@Override
		public SearchRequest appendInclusiveSingleValueClause(Option value, SearchRequest searchRequest) {
			return appendAndClause(value, searchRequest, this);
		}

		@Override
		public SearchRequest appendExclusiveMultiValueClause(Iterable<? extends Option> values, SearchRequest searchRequest) {
			return appendAndNotClauses(values, searchRequest, this);
		}
	}
}
