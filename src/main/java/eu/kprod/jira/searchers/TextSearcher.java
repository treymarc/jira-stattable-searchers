package eu.kprod.jira.searchers;

import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;

import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.issue.customfields.SingleValueCustomFieldValueProvider;
import com.atlassian.jira.issue.customfields.searchers.SimpleAllTextCustomFieldSearcherClauseHandler;
import com.atlassian.jira.issue.customfields.searchers.information.CustomFieldSearcherInformation;
import com.atlassian.jira.issue.customfields.searchers.renderer.CustomFieldRenderer;
import com.atlassian.jira.issue.customfields.searchers.transformer.CustomFieldInputHelper;
import com.atlassian.jira.issue.customfields.searchers.transformer.FreeTextCustomFieldSearchInputTransformer;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.index.DocumentConstants;
import com.atlassian.jira.issue.index.indexers.FieldIndexer;
import com.atlassian.jira.issue.index.indexers.impl.SortableTextCustomFieldIndexer;
import com.atlassian.jira.issue.search.LuceneFieldSorter;
import com.atlassian.jira.issue.statistics.StatisticsMapper;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operator.OperatorClasses;
import com.atlassian.jira.jql.query.FreeTextClauseQueryFactory;
import com.atlassian.jira.jql.validator.FreeTextFieldValidator;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.FieldVisibilityManager;

/**
 * Modifies standard searcher to use the natural order for strings.
 * 
 * @author Jozef Kotlar
 * @author Peter Csiba
 */
public class TextSearcher extends AbstractSearcher {

	public TextSearcher(FieldVisibilityManager fieldVisibilityManager, JqlOperandResolver jqlOperandResolver,
			CustomFieldInputHelper customFieldInputHelper, JiraAuthenticationContext jiraAuthenticationContext) {
		super(jqlOperandResolver, customFieldInputHelper, fieldVisibilityManager, jiraAuthenticationContext);

	}

	@Override
	public LuceneFieldSorter<?> getSorter(CustomField customField) {
		return new TextFieldNaturalSorter(customField.getId());
	}

	@Override
	public StatisticsMapper<?> getStatisticsMapper(CustomField customField) {
		return new FreeTextStatisticsMapper(customField, jiraAuthenticationContext, customFieldInputHelper);
	}

	@Override
	public void init(CustomField field) {

		super.searcherInformation = new CustomFieldSearcherInformation(field.getId(), field.getNameKey(),
				Collections.<FieldIndexer> singletonList(new SortableTextCustomFieldIndexer(fieldVisibilityManager, field,
						DocumentConstants.LUCENE_SORTFIELD_PREFIX)), new AtomicReference<CustomField>(field));
		super.searchRenderer = new CustomFieldRenderer(field.getClauseNames(), getDescriptor(), field, new SingleValueCustomFieldValueProvider(),
				super.fieldVisibilityManager);
		super.searchInputTransformer = new FreeTextCustomFieldSearchInputTransformer(field, field.getClauseNames(),
				super.searcherInformation.getId(), super.customFieldInputHelper);
		super.customFieldSearcherClauseHandler = new SimpleAllTextCustomFieldSearcherClauseHandler(new FreeTextFieldValidator(field.getId(),
				super.jqlOperandResolver), new FreeTextClauseQueryFactory(super.jqlOperandResolver, field.getId()), OperatorClasses.TEXT_OPERATORS,
				JiraDataTypes.TEXT);
	}

}
