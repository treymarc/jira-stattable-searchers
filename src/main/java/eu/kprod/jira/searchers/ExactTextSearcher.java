package eu.kprod.jira.searchers;

import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;

import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.issue.customfields.SingleValueCustomFieldValueProvider;
import com.atlassian.jira.issue.customfields.searchers.SimpleAllTextCustomFieldSearcherClauseHandler;
import com.atlassian.jira.issue.customfields.searchers.SimpleCustomFieldSearcherClauseHandler;
import com.atlassian.jira.issue.customfields.searchers.information.CustomFieldSearcherInformation;
import com.atlassian.jira.issue.customfields.searchers.renderer.CustomFieldRenderer;
import com.atlassian.jira.issue.customfields.searchers.transformer.CustomFieldInputHelper;
import com.atlassian.jira.issue.customfields.searchers.transformer.ExactTextCustomFieldSearchInputTransformer;
import com.atlassian.jira.issue.customfields.searchers.transformer.FreeTextCustomFieldSearchInputTransformer;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.index.DocumentConstants;
import com.atlassian.jira.issue.index.indexers.FieldIndexer;
import com.atlassian.jira.issue.index.indexers.impl.SortableTextCustomFieldIndexer;
import com.atlassian.jira.issue.search.ClauseNames;
import com.atlassian.jira.issue.search.LuceneFieldSorter;
import com.atlassian.jira.issue.statistics.StatisticsMapper;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operator.OperatorClasses;
import com.atlassian.jira.jql.query.ActualValueCustomFieldClauseQueryFactory;
import com.atlassian.jira.jql.query.FreeTextClauseQueryFactory;
import com.atlassian.jira.jql.util.SimpleIndexValueConverter;
import com.atlassian.jira.jql.validator.ExactTextCustomFieldValidator;
import com.atlassian.jira.jql.validator.FreeTextFieldValidator;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.FieldVisibilityManager;

/**
 * Modified standard searcher to use the natural order for strings.
 * 
 * @author Jozef Kotlar
 * @author Peter Csiba
 */
public class ExactTextSearcher extends AbstractSearcher {

	public ExactTextSearcher(FieldVisibilityManager fieldVisibilityManager, JqlOperandResolver jqlOperandResolver,
			CustomFieldInputHelper customFieldInputHelper, JiraAuthenticationContext jiraAuthenticationContext) {
		super(jqlOperandResolver, customFieldInputHelper, fieldVisibilityManager, jiraAuthenticationContext);

	}

	@Override
	public LuceneFieldSorter<?> getSorter(CustomField customField) {
		return new TextFieldNaturalSorter(customField.getId());
	}

	@Override
	public StatisticsMapper<?> getStatisticsMapper(CustomField customField) {
		return new ExactTextStatisticsMapper(customField, jiraAuthenticationContext, customFieldInputHelper);
	}


	@Override
	public void init(CustomField field) {
		final ClauseNames names = field.getClauseNames();
		final FieldIndexer indexer = new ExactTextCustomFieldIndexer(super.fieldVisibilityManager, field);
		super.searcherInformation = new CustomFieldSearcherInformation(field.getId(), field.getNameKey(), Collections.<FieldIndexer> singletonList(indexer), new AtomicReference<CustomField>(field));
		super.searchRenderer = new CustomFieldRenderer(names, getDescriptor(), field, new SingleValueCustomFieldValueProvider(),
				super.fieldVisibilityManager);
		super.searchInputTransformer = new ExactTextCustomFieldSearchInputTransformer(field, names, super.searcherInformation.getId(),
				customFieldInputHelper);	
		super.customFieldSearcherClauseHandler = new SimpleAllTextCustomFieldSearcherClauseHandler(new ExactTextFieldValidator(field.getId(),
				super.jqlOperandResolver), new ExactValueCustomFieldClauseQueryFactory(field.getId(), super.jqlOperandResolver, new SimpleIndexValueConverter(false), false), OperatorClasses.NON_RELATIONAL_OPERATORS,
				JiraDataTypes.TEXT);
		
	}

}
