package eu.kprod.jira.searchers;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

import com.atlassian.jira.issue.customfields.SortableCustomFieldSearcher;
import com.atlassian.jira.issue.customfields.searchers.AbstractInitializationCustomFieldSearcher;
import com.atlassian.jira.issue.customfields.searchers.CustomFieldSearcherClauseHandler;
import com.atlassian.jira.issue.customfields.searchers.information.CustomFieldSearcherInformation;
import com.atlassian.jira.issue.customfields.searchers.transformer.CustomFieldInputHelper;
import com.atlassian.jira.issue.customfields.statistics.CustomFieldStattable;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.searchers.information.SearcherInformation;
import com.atlassian.jira.issue.search.searchers.renderer.SearchRenderer;
import com.atlassian.jira.issue.search.searchers.transformer.SearchInputTransformer;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.FieldVisibilityManager;

/**
 * An abstract class with CustomFieldSearcher.
 * 
 * @author Marc Trey
 */
abstract class AbstractSearcher extends AbstractInitializationCustomFieldSearcher implements CustomFieldStattable, SortableCustomFieldSearcher {

	protected volatile JiraAuthenticationContext		jiraAuthenticationContext;
	protected volatile FieldVisibilityManager			fieldVisibilityManager;
	protected volatile JqlOperandResolver				jqlOperandResolver;
	protected volatile CustomFieldInputHelper			customFieldInputHelper;

	protected volatile CustomFieldSearcherInformation	searcherInformation;
	protected volatile SearchInputTransformer			searchInputTransformer;
	protected volatile SearchRenderer					searchRenderer;
	protected volatile CustomFieldSearcherClauseHandler	customFieldSearcherClauseHandler;

	public AbstractSearcher(JqlOperandResolver jqlOperandResolver, CustomFieldInputHelper customFieldInputHelper,
			FieldVisibilityManager fieldVisibilityManager, JiraAuthenticationContext jiraAuthenticationContext) {
		this.jiraAuthenticationContext = jiraAuthenticationContext;
		this.fieldVisibilityManager = fieldVisibilityManager;
		this.jqlOperandResolver = jqlOperandResolver;
		this.customFieldInputHelper = notNull("customFieldInputHelper", customFieldInputHelper);
	}

	@Override
	public final SearcherInformation<CustomField> getSearchInformation() {
		if (searcherInformation == null) {
			throw new IllegalStateException("Attempt to retrieve SearcherInformation off uninitialised custom field searcher.");
		}
		return searcherInformation;
	}

	@Override
	public final SearchInputTransformer getSearchInputTransformer() {
		if (searchInputTransformer == null) {
			throw new IllegalStateException("Attempt to retrieve searchInputTransformer off uninitialised custom field searcher.");
		}
		return searchInputTransformer;
	}

	@Override
	public final SearchRenderer getSearchRenderer() {
		if (searchRenderer == null) {
			throw new IllegalStateException("Attempt to retrieve searchRenderer off uninitialised custom field searcher.");
		}
		return searchRenderer;
	}

	@Override
	public final CustomFieldSearcherClauseHandler getCustomFieldSearcherClauseHandler() {
		if (customFieldSearcherClauseHandler == null) {
			throw new IllegalStateException("Attempt to retrieve customFieldSearcherClauseHandler off uninitialised custom field searcher.");
		}
		return customFieldSearcherClauseHandler;
	}

	/**
	 * This is the first time the searcher knows what its ID and names are.
	 * 
	 * @param field
	 *            the Custom Field for this searcher
	 */
	@Override
	public abstract void init(CustomField field);

}
