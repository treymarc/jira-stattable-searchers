package eu.kprod.jira.searchers;

import java.util.Comparator;

import org.apache.commons.collections.ComparatorUtils;
import org.apache.commons.lang.StringUtils;

import com.google.common.base.Predicate;

public enum NaturalWithNullComparator implements Comparator<String> {

	// 255 char is enough
	CASE_SENSITIVE_ORDER(true, 255), CASE_INSENSITIVE_ORDER(false, 255);
	// CASE_INSENSITIVE_NOLIMIT_ORDER(false,-1);
	// CASE_INSENSITIVE_LIMI8_ORDER(false,8);

	private static final Predicate<Integer>	NUMERIC_PREDICATE	= new Predicate<Integer>() {
																	@Override
																	public boolean apply(Integer input) {
																		return Character.isDigit(input);
																	}
																};

	/**
	 * Compares digit strings numerically. Leading zeroes do not affect the
	 * result.
	 * 
	 * @param digits1
	 *            first digit string
	 * @param digits2
	 *            second digit string
	 * @return positive number if digits1 is bigger than digits2, 0 if they are
	 *         equal, and negative number if digits1 is smaller than digits2
	 */
	private static int compareNumerically(String digits1, String digits2) {
		digits1 = StringUtils.stripStart(digits1, "0");
		digits2 = StringUtils.stripStart(digits2, "0");

		int str1ChunkLength = digits1.length();
		int result = str1ChunkLength - digits2.length();
		// if equal, the first different number is used to compare, otherwise
		// the longer digit chunk is bigger
		if (result == 0) {
			for (int i = 0; i < str1ChunkLength; i++) {
				result = digits1.codePointAt(i) - digits2.codePointAt(i);
				if (result != 0) {
					return result;
				}
			}
		}
		return result;
	}

	/**
	 * Returns either a chunk of digits or a chunk of non-digits.
	 * 
	 * @param str
	 *            String
	 * @param marker
	 *            current position of the string being processed
	 * @return a chunk of digits or a chunk of non-digits
	 */
	private static String getChunk(String str, int marker) {
		if (Character.isDigit(str.codePointAt(marker))) {
			return getChunk(str, marker, NUMERIC_PREDICATE);
		} else {
			return getChunk(str, marker, com.google.common.base.Predicates.not(NUMERIC_PREDICATE));
		}
	}

	/**
	 * Returns a chunk based on the given predicate.
	 * 
	 * @param str
	 *            String
	 * @param marker
	 *            current position of the string being processed
	 * @param predicate
	 *            chunk ends when the predicate returns <tt>false</tt>
	 * @return chunk
	 */
	private static String getChunk(String str, int marker, Predicate<Integer> predicate) {
		int endIndex = marker;
		int strLength = str.length();
		int codePoint;
		while (endIndex < strLength) {
			codePoint = str.codePointAt(endIndex);
			if (!predicate.apply(codePoint)) {
				break;
			}
			endIndex += Character.charCount(codePoint);
		}
		return str.substring(marker, endIndex);
	}

	private final Comparator<String>	stringComparator;
	private int							comparLimit	= 0;

	public int getComparLimit() {
		return comparLimit;
	}

	@SuppressWarnings("unchecked")
	private NaturalWithNullComparator(boolean caseSensitive, int limit) {
		stringComparator = caseSensitive ? ComparatorUtils.NATURAL_COMPARATOR : String.CASE_INSENSITIVE_ORDER;
		comparLimit = limit;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int compare(String istr1, String istr2) {
		if ((istr1 == null) || (istr2 == null)) {
			if (istr1 == istr2) {
				return 0;
			} else if (istr1 != null) {
				return 1;
			} else {
				return -1;
			}
		}

		if (istr1.length() > comparLimit) {
			istr1 = istr1.substring(0, comparLimit);
		}

		if (istr2.length() > comparLimit) {
			istr2 = istr2.substring(0, comparLimit);
		}

		int str1Length = istr1.length();
		int str2Length = istr2.length();
		int str1Marker = 0;
		int str2Marker = 0;

		while (str1Marker < str1Length && str2Marker < str2Length) {
			String str1Chunk = getChunk(istr1, str1Marker);
			str1Marker += str1Chunk.length();

			String str2Chunk = getChunk(istr2, str2Marker);
			str2Marker += str2Chunk.length();

			// if both chunks contain numeric characters, sort them numerically
			int result;
			if (Character.isDigit(str1Chunk.codePointAt(0)) && Character.isDigit(str2Chunk.codePointAt(0))) {
				result = compareNumerically(str1Chunk, str2Chunk);
			} else {
				str1Chunk = StringUtils.strip(str1Chunk);
				str2Chunk = StringUtils.strip(str2Chunk);
				result = stringComparator.compare(str1Chunk, str2Chunk);
			}

			if (result != 0) {
				return result;
			}

		}

		return str1Length - str2Length;
	}
}
